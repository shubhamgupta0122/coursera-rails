module GreeterHelper
	def formatted_time(time)
		time.strftime("%d-%b-%Y %I:%M%p IST - %s")
	end
end
